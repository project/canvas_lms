<?php

namespace Drupal\canvas_lms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form to configure the settings for the Canvas LMS integration.
 */
class CanvasLmsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'canvas_lms_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'canvas_lms.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL): array {
    $config = $this->config('canvas_lms.settings');

    $form['institution'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Institution'),
      '#description' => $this->t("Enter your institution's name as it appears in your CanvasApi URL. <br>i.e. https://<strong>your-institutions-name</strong>.instructure.com"),
      '#default_value' => $config->get('institution'),
    ];

    $form['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#options' => [
        'test' => $this->t('Test'),
        'beta' => $this->t('Beta'),
        'production' => $this->t('Production'),
      ],
      '#default_value' => $config->get('environment'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $this->config('canvas_lms.settings')
      ->set('institution', $form_state->getValue('institution'))
      ->set('environment', $form_state->getValue('environment'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
